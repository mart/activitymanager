/*
 *   Copyright 2012 Marco Martin <mart@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2 or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import QtQuick 1.1
import org.kde.plasma.core 0.1 as PlasmaCore
import org.kde.plasma.components 0.1 as PlasmaComponents
import org.kde.plasma.extras 0.1 as PlasmaExtras
import org.kde.qtextracomponents 0.1 as QtExtras

Item {
    id: root

    property int minimumWidth: theme.defaultFont.mSize.width * 30
    property int minimumHeight: 200
    property int preferredWidth: minimumWidth
    property int preferredHeight: theme.defaultFont.mSize.height * 30

    property bool popupShowing: false

    Component.onCompleted: {
        var configString = String(plasmoid.readConfig("PinnedActivities"))
        var itemsStrings = configString.split(",")

        for (var i = 0; i < itemsStrings.length; i++) {
            if (itemsStrings[i] != "" && itemsStrings[i] != "undefined") {
                pinnedActivities.append({"id": itemsStrings[i]})
            }
        }
    }

    ListModel {
        id: pinnedActivities

        function addActivity(activity) {
            if (activity != "" && activity != "undefined" && !containsActivity(activity)) {
                append({"id": activity})
                save()
            }
        }

        function removeActivity(activity) {
            for (var i = 0; i < count; ++i) {
                if (get(i).id == activity) {
                    remove(i)
                    save()
                    return
                }
            }
        }

        function containsActivity(activity) {
            for (var i = 0; i < count; ++i) {
                if (get(i).id == activity) {
                    return true
                }
            }
            return false
        }

        function save() {
            var string
            for (var i = 0; i < count; ++i) {
                string += get(i).id + ","
            }
            plasmoid.writeConfig("PinnedActivities", string)
        }
    }

    PlasmaCore.DataSource {
        id: activitySource
        engine: "org.kde.activities"
        onSourceAdded: {
            if (source != "Status") {
                connectSource(source)
            }
        }
        Component.onCompleted: {
            connectedSources = sources.filter(function(val) {
                return val != "Status";
            })
        }
    }

    property Component compactRepresentation: Component {
        Item {
            width: panelFlow.width
            property int minimumWidth: panelFlow.implicitWidth
            property int minimumHeight: 1

            QtExtras.MouseEventListener {
                id: wheelListener
                property int selected

                anchors {
                    fill:parent
                    leftMargin: activitiesIcon.width
                }
                width: childrenRect.width
                onWheelMoved: {
                    if (wheel.delta < 0) {
                        selected = (selected+1) % pinnedActivities.count
                    } else {
                        selected = (pinnedActivities.count + selected - 1) % pinnedActivities.count
                    }
                    //FIXME: utterly inefficient, maybe use  real tabbar
                    for (var i = 0; i < activityButtons.children.length; ++i) {
                        if (activityButtons.children[i].index == selected) {
                            activityButtons.children[i].checked = true
                        }
                    }
                }
            }

            Row {
                id: panelFlow
                spacing: 0
                anchors {
                    top: parent.top
                    bottom:parent.bottom
                    topMargin: 2
                    bottomMargin: 2
                }

                PlasmaCore.IconItem {
                    id: activitiesIcon
                    width: height+1
                    height: parent.height
                    source: "preferences-activities"
                }

                PlasmaComponents.ButtonRow {
                    id: activityButtons
                    anchors.verticalCenter: parent.verticalCenter
                    spacing: 0

                    Repeater {
                        model: pinnedActivities
                        delegate: PlasmaComponents.ToolButton {
                            property int index: model["index"]
                            width: height+1
                            height: panelFlow.height
                            flat: false
                            checked: activitySource.data[id]["Current"] == true
                            onCheckedChanged: {
                                if (checked) {
                                    var service = activitySource.serviceForSource(id)
                                    var operation = service.operationDescription("setCurrent")
                                    service.startOperationCall(operation)
                                    wheelListener.selected = index
                                }
                            }
                        }
                    }
                }
            }
        }
    }
 
    //This because property bindings on plasmoid don't work
    Connections {
        target: plasmoid
        onPopupEvent: root.popupShowing = plasmoid.popupShowing
    }

    PlasmaExtras.ConditionalLoader {
        anchors.fill: parent
        when: root.popupShowing
        source: Qt.resolvedUrl("ActivityView.qml")
    }
    
}

