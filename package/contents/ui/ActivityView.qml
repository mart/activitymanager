/*
 *   Copyright 2012 Marco Martin <mart@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2 or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import QtQuick 1.1
import org.kde.plasma.core 0.1 as PlasmaCore
import org.kde.plasma.components 0.1 as PlasmaComponents
import org.kde.plasma.extras 0.1 as PlasmaExtras

Item {
    id: root

    PlasmaComponents.ToolBar {
        id: toolBar
        tools: PlasmaComponents.ToolBarLayout {
            PlasmaComponents.ToolButton {
                iconSource: "list-add"
                flat: false
                onClicked: {
                    var service = activitySource.serviceForSource("Status")
                    var operation = service.operationDescription("add")
                    service.startOperationCall(operation)
                }
            }
            PlasmaComponents.TextField {
                id: actionsToolBar
                placeholderText: i18n("Search...")
                clearButtonShown: true
            }
        }
    }
    PlasmaExtras.ScrollArea {
        anchors {
            top: toolBar.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
        GridView {
            id: mainView

            cellWidth: theme.defaultFont.mSize.width * 30
            cellHeight: cellWidth / 1.6
            flow: width >= height * 2 ? GridView.TopToBottom : GridView.LeftToRight 

            property int deleteDialogOpenedAtIndex: 01

            model:  PlasmaCore.SortFilterModel {
                sourceModel: PlasmaCore.DataModel {
                    dataSource: activitySource
                }
                filterRole: "Name"
                filterRegExp: ".*"+actionsToolBar.text+".*"
            }
            delegate: ActivityDelegate {
                width: mainView.cellWidth
                height: mainView.cellHeight
            }
        }
    }
}

